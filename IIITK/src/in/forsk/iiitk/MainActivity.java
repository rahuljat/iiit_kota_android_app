
package in.forsk.iiitk;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;


public class MainActivity extends ActionBarActivity {

	private RelativeLayout lib;
	private RelativeLayout knowcenter;
	private RelativeLayout auditoriam;
	private RelativeLayout carparking;
	private RelativeLayout footfield;
	private RelativeLayout boyresidence;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        lib = (RelativeLayout) findViewById(R.id.library);
        knowcenter = (RelativeLayout) findViewById(R.id.knowledge_center);
        auditoriam = (RelativeLayout) findViewById(R.id.auditoriam);
        carparking = (RelativeLayout) findViewById(R.id.carparking);
        footfield = (RelativeLayout) findViewById(R.id.footballfield);
        boyresidence = (RelativeLayout) findViewById(R.id.boyresidence);
        
        
        
        lib.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent  = new Intent(android.content.Intent.ACTION_VIEW , Uri.parse("https://www.google.co.in/maps/place/MNIT+Library/@26.8620353,75.8066222,699m/data=!3m1!1e3!4m2!3m1!1s0x396db6778e94d8f5:0xb628093737290f4b!6m1!1e1"));
					startActivity(intent);
					
			}
		});
        
    

    
    
    
        knowcenter.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
				Intent intent  = new Intent(android.content.Intent.ACTION_VIEW , Uri.parse("https://www.google.co.in/maps/place/MNIT+Design+Center/@26.8642943,75.8085329,699m/data=!3m2!1e3!4b1!4m2!3m1!1s0x396db679b859206f:0x8f858ef2e9834e5f"));
				startActivity(intent);
				
		}
	});
    

    
    

    
    
    
    
        auditoriam.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
				Intent intent  = new Intent(android.content.Intent.ACTION_VIEW , Uri.parse("https://www.google.co.in/maps/place/MNIT+Design+Center/@26.8642943,75.8085329,699m/data=!3m2!1e3!4b1!4m2!3m1!1s0x396db679b859206f:0x8f858ef2e9834e5f"));
				startActivity(intent);
				
		}
	});
    

    
    
 
        carparking.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
				Intent intent  = new Intent(android.content.Intent.ACTION_VIEW , Uri.parse("https://www.google.co.in/maps/place/MNIT+Parking,+Jhalana,+Malviya+Nagar,+Jaipur,+Rajasthan+302017/@26.8652329,75.8074865,203m/data=!3m1!1e3!4m7!1m4!3m3!1s0x396db678620461fb:0x6f01cb0380d75466!2sMNIT+Parking,+Jhalana,+Malviya+Nagar,+Jaipur,+Rajasthan+302017!3b1!3m1!1s0x396db678620461fb:0x6f01cb0380d75466"));
				startActivity(intent);
				
		}
	});
 
 
    
    
 
        footfield.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
				Intent intent  = new Intent(android.content.Intent.ACTION_VIEW , Uri.parse("https://www.google.co.in/maps/search/MNIT+Playground,+Jhalana,+Jaipur,+Rajasthan/@26.8598107,75.8124151,406m/data=!3m2!1e3!4b1"));
				startActivity(intent);
				
		}
	});
 
 
 
 
 
        boyresidence.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
				Intent intent  = new Intent(android.content.Intent.ACTION_VIEW , Uri.parse("https://www.google.co.in/maps/place/Aurobindo+Hostel/@26.8627701,75.8193365,348m/data=!3m1!1e3!4m2!3m1!1s0x396db66f93dbc999:0x7abb2c8b74c0026a!6m1!1e1"));
				startActivity(intent);
				
		}
	});
 
 
 
 
    
    }
   // @Override
   // public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
    //    getMenuInflater().inflate(R.menu.main, menu);
     //   return true;
   // }

   // @Override
   // public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
      //  int id = item.getItemId();
      //  if (id == R.id.action_settings) {
       //     return true;
       // }
       // return super.onOptionsItemSelected(item);
    }
//}
